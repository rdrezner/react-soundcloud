import * as actionTypes from '../constants/actionTypes'

export const setTracks = (tracks) => {
  return {
    type: actionTypes.TRACKS_SET,
    tracks
  }
}

export const fetchUsers = () => {
    return (dispatch) => {
      fetch('https://restcountries-v1.p.mashape.com/all', {
        headers: {
          'X-Mashape-Key': 'hQGSwXcywrmshZUgMVsrMajkVLcAp1cAzWRjsnRxsYSt3EJ1Kf',
          'Accept': 'application/json'
        }
      })
        .then((response) => response.json())
        .then((data) => {
          dispatch(setTracks(data));
        });
    };
}