import React from 'react';

const styles = {
  fontFamily: 'Roboto, sans-serif'
}

const App = ({children}) => {
  return <div style={styles}>
    {children}
  </div>;
}

export default App;