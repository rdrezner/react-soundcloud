import React, {Component} from 'react';
import Drawer from 'material-ui/Drawer';
import RaisedButton from 'material-ui/RaisedButton';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import Toggle from 'material-ui/Toggle';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const peopleFilterToggleStrategy = (filters, selectedFilter) => {
  const toggledFilters = multiselectToggleStrategy(filters, selectedFilter);
  if(toggledFilters.length < 1) {
    return selectedFilter === 'includeEmployees' ? ['includeCandidates'] : ['includeEmployees'];
  }
  return toggledFilters;
}

const multiselectToggleStrategy = (filters, selectedFilter) => (
  (filters.indexOf(selectedFilter) > -1) 
    ? filters.filter((element) => element != selectedFilter) 
    : [...filters, selectedFilter]
);

const filterGroups = {
  peopleFilter: {
    values: ['includeEmployees', 'includeCandidates'],
    initialValues: ['includeEmployees'],
    toggleStrategy: peopleFilterToggleStrategy,
  },
  locationFilter: {
    values: ['PPNT', 'LPP Reduta', 'LPP Długie Ogrody', 'Nordea', 'VPS'],
    initialValues: [],
    toggleStrategy: multiselectToggleStrategy,
  }
};

export default class FiltersComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      selectedFilters: Object.keys(filterGroups)
        .map((key) => [key, filterGroups[key].initialValues])
        .reduce((acc, curr) => ({...acc, [curr[0]]: curr[1]}), {}),
    };
  }

  handleToggle = () => this.setState({open: !this.state.open});

  handleFilterToggle = (filterGroup, toggledValue) => {
    const newSelectedFilters = {
      ...this.state.selectedFilters, 
      [filterGroup]: filterGroups[filterGroup].toggleStrategy.call(this, this.state.selectedFilters[filterGroup], toggledValue)
    };
    this.setState({
      selectedFilters: newSelectedFilters,
    });
  };

  render() {
    const { selectedFilters } = this.state;
    return (
      <div>
        <RaisedButton
          label="Toggle Drawer"
          onClick={this.handleToggle}
        />
        <Drawer 
          openSecondary
          docked={false}
          open={this.state.open}
          onRequestChange={(open) => this.setState({open})}
        >
          {Object.keys(filterGroups).map((filterGroup) => (
            <div key={filterGroup}>
              <List>
                <Subheader>{filterGroup}</Subheader>
                {filterGroups[filterGroup].values.map((filterValue, index) => (
                  <ListItem
                    key={index}
                    leftCheckbox={
                      <Checkbox 
                        checked={selectedFilters[filterGroup].indexOf(filterValue) > -1}
                        onClick={this.handleFilterToggle.bind(this, filterGroup, filterValue)}
                      />
                    }
                    primaryText={filterValue}
                />
                ))}
              </List>
              <SelectField
                multiple={true}
                hintText={filterGroup}
                value={selectedFilters[filterGroup]}
                onChange={(event, index, values) => {
                  console.log(values);
                  this.setState({
                    selectedFilters: {...selectedFilters, [filterGroup]: values},
                  });
                }}
              >
                {filterGroups[filterGroup].values.map((filterValue, index) => (
                  <MenuItem
                    key={index}
                    insetChildren
                    checked={selectedFilters[filterGroup].indexOf(filterValue) > -1}
                    value={filterValue}
                    primaryText={filterValue}
                  />
                ))
                }
              </SelectField>
            </div>
          )
          )}
        </Drawer>
      </div>
    );
  }
}