import React, {Component} from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

const persons = [
  {value: 0, name: 'Oliver Hansen'},
  {value: 1, name: 'Van Henry'},
  {value: 2, name: 'April Tucker'},
  {value: 3, name: 'Ralph Hubbard'},
  {value: 4, name: 'Omar Alexander'},
  {value: 5, name: 'Carlos Abbott'},
  {value: 6, name: 'Miriam Wagner'},
  {value: 7, name: 'Bradley Wilkerson'},
  {value: 8, name: 'Virginia Andrews'},
  {value: 9, name: 'Kelly Snyder'},
];

/**
 * The rendering of selected items can be customized by providing a `selectionRenderer`.
 */
export default class SelectFieldExampleSelectionRenderer extends Component {
  state = {
    values: [],
  };

  handleChange = (event, index, values) => this.setState({values});

  menuElementClicked = (value, filterGroup) => {
    console.log(this.state, value, filterGroup);
    const { values } = this.state;
    if (values.indexOf(value) < 0) {
      this.setState({values: [...values, value]})
    } else {
      this.setState({values: values.filter((element) => element !== value)});
    }
  }

  selectionRenderer = (values) => {
    // switch (values.length) {
    //   case 0:
    //     return '';
    //   case 1:
    //     return values[0];
    //   default:
    //     return `${values.length} filters selected`;
    // }
    return (
      <div style={{textOverflow: 'ellipsis', overflow: 'hidden'}}>
        {values.join(', ')}
      </div>);
  }

  menuItems(persons) {
    return persons.map((person) => (
      <MenuItem
        key={person.value}
        insetChildren
        checked={this.state.values.indexOf(person.value) > -1}
        value={person.value}
        primaryText={person.name}
        onClick={this.menuElementClicked.bind(this, person.value, "location")}
      />
    ));
  }

  render() {
    return (
      <div>
        {/* <SelectField
          multiple
          hintText="Select a name"
          value={this.state.values}
          onChange={this.handleChange}
          selectionRenderer={this.selectionRenderer}
        >
          <MenuItem
            insetChildren
            checked={this.state.values.indexOf("includeCandidates") > -1}
            value="includeCandidates"
            primaryText="Candidates"
            onClick={this.menuElementClicked.bind(null, "includeCandidates", "people")}
          />
          <Divider />
          {this.menuItems(persons)}
        </SelectField> */}

        <IconMenu
          iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
          clickCloseDelay={0}
        >
          <MenuItem
            insetChildren
            checked={this.state.values.indexOf("includeCandidates") > -1}
            value="includeCandidates"
            primaryText="Candidates"
            onClick={this.menuElementClicked.bind(null, "includeCandidates", "people")}
          />
          <Divider />
          {this.menuItems(persons)}
        </IconMenu>
      </div>
    );
  }
}