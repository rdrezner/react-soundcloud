import React, { Component } from 'react'

const tabs = ['a', 'b', 'c']

class Tab extends Component {
  constructor(props) {
    super(props)
    this.onClick = this.onClick.bind(this)
  }
  onClick() {
    this.props.onClick(this.props.id)
  }
  render() {
    const { style } = this.props;
    return (
      <div onClick={this.onClick} style={style}>
        {this.props.children}
      </div>
    );
  }
}

export default class RefsComponent extends Component {
  constructor(props) {
    super(props)
    this.activeRef = null
    this.state = {
      activeEl: 'b'
    }
    this.getRef = this.getRef.bind(this)
    this.setActive = this.setActive.bind(this)
    this.getRef = this.getRef.bind(this)
  }

  getRef(el) {
    if (el) {
      this.activeRef = el
    }
  }

  setActive(elem) {
    this.setState({ activeEl: elem })
  }

  getTabProps(el) {
    return {
      key: el,
      id: el,
      onClick: this.setActive
    };
  }

  getTab(el) {
    const refProp = el === this.state.activeEl ? {ref: this.getRef} : {}
    return (
      <Tab style={{ color: el === this.state.activeEl ? "red" : "black" }} {...this.getTabProps(el)} {...refProp}>
        {el}
      </Tab>
    );
  }

  render() {
    return (
      <div>
        {tabs.map(
          el => this.getTab(el)
        )}
      </div>
    );
  }
}
