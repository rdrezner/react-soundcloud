import React, { Component } from 'react';
import Scrollspy from 'react-scrollspy';
import ReactTooltip from 'react-tooltip';
import {
  Link,
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller
} from 'react-scroll';

const lorem = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus fermentum arcu id fringilla. Pellentesque consequat pharetra orci, vitae bibendum neque euismod non. Phasellus elit tortor, lacinia non nisi quis, condimentum porttitor sem. Etiam tempus est orci, vitae porttitor tortor placerat et. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean porttitor lacus ornare metus dignissim, sit amet finibus mi auctor. Nulla vitae quam placerat, eleifend urna non, pulvinar nibh. Fusce laoreet metus et dui faucibus pulvinar. Nullam eleifend libero venenatis lectus bibendum consectetur. Nam lectus lacus, posuere at quam a, semper scelerisque enim. Proin vehicula id neque id iaculis.

Mauris pellentesque nulla in justo facilisis, aliquam feugiat sem rhoncus. Etiam tincidunt est nec accumsan ultrices. Ut posuere, urna ac dignissim tristique, est magna pharetra nibh, in laoreet massa dolor et est. Etiam accumsan vitae magna eget auctor. Sed sollicitudin nunc nec scelerisque rutrum. Sed nisi magna, iaculis eget urna vel, mollis accumsan augue. Praesent et lectus gravida, varius purus et, porta nisl. Etiam porta urna nisl, sed molestie urna ornare sed. Pellentesque in eros at tellus faucibus posuere. In fermentum, nibh ac lacinia convallis, leo turpis bibendum nulla, id pretium libero eros eu justo. Nullam interdum, neque a mattis ultricies, tortor est scelerisque nulla, nec maximus nisi purus nec felis. Sed laoreet libero sit amet finibus mollis. Maecenas vehicula blandit velit eu volutpat. Ut eu rhoncus diam.

Integer congue gravida porta. Quisque dui ante, auctor vitae lobortis a, maximus quis mauris. Nam dolor turpis, molestie vel auctor a, ultrices sit amet risus. Proin mauris diam, euismod vitae lacus quis, iaculis tincidunt nisi. Donec aliquam mollis tortor vel dapibus. Donec iaculis facilisis nisl. Cras consequat ut nisi ac malesuada. Phasellus varius magna risus, at mattis risus mollis non. Aliquam vulputate lacus dolor, id ultricies dui efficitur non. Morbi non blandit metus. Phasellus finibus fringilla arcu, ac pulvinar urna mollis nec.`;

const sectionStyle = {};
const linkStyle = {};

export default class ScrollspyComponent extends Component {
  render() {
    return (
      <div
        id="container"
        style={{
          display: 'flex',
          position: 'relative',
          height: '50vh',
          overflow: 'scroll'
        }}
      >
        <ul style={{ flexBasis: '20%' }}>
          <li>
            <Link
              activeClass="active"
              className="test1"
              to="test1"
              spy={true}
              smooth={true}
              duration={500}
              containerId="container"
            >
              Test 1
            </Link>
          </li>
          <li>
            <Link
              activeClass="active"
              className="test2"
              to="test2"
              spy={true}
              smooth={true}
              duration={500}
              containerId="container"
            >
              Test 2
            </Link>
          </li>
          <li>
            <Link
              activeClass="active"
              className="test3"
              to="test3"
              spy={true}
              smooth={true}
              duration={500}
              containerId="container"
            >
              Test 3
            </Link>
          </li>
        </ul>
        <div
          id="container2"
          style={{
            flexBasis: '80%'
          }}
        >
          <Element name="test1" className="element">
            <div>
              <span>section 1</span>
              <p>{lorem}</p>
            </div>
          </Element>
          <Element name="test2" className="element">
            <section style={sectionStyle}>
              <h1>section 2</h1>
              <p>{lorem}</p>
            </section>
          </Element>
          <Element name="test3" className="element">
            <section style={sectionStyle}>
              <h1>section 3</h1>
              <p>{lorem}</p>
            </section>
          </Element>
        </div>
      </div>
    );
  }
}
