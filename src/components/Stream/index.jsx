import React from 'react';
import { connect } from 'react-redux';
import Stream from './presenter';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions';

const mapStateToProps = state => {
  const tracks = state.trackState;
  return {
    tracks
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onFetchUsers: bindActionCreators(actions.fetchUsers, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Stream);
