import React from 'react';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import RaisedButton from 'material-ui/RaisedButton';
import ListIcon from 'material-ui/svg-icons/action/list';
import TilesIcon from 'material-ui/svg-icons/action/view-module';

const styles = {
  container: {
    borderColor: 'red',
    borderStyle: 'solid',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  left: {
    borderColor: 'green',
    borderStyle: 'solid',
    flex: '1',
    display: 'flex',
    justifyContent: 'flex-start',
  },
  center: {
    borderColor: 'blue',
    borderStyle: 'solid',
    flex: '2',
    display: 'flex',
    justifyContent: 'center',
  },
  right: {
    borderColor: 'pink',
    borderStyle: 'solid',
    flex: '1',
    display: 'flex',
    justifyContent: 'flex-end',
  }
};

const sorts = [
  {
    dir: 'asc',
    column: 'name',
  },
  {
    dir: 'desc',
    column: 'surname',
  },
  {
    dir: 'none',
    column: 'email',
  },
];

const Stream = ({ tracks = [], onFetchUsers }) => {
  const sortUrl = sorts.reduce((previous, current) => `${previous + current.column},${current.dir},`, 'sort=');
  const sortUrl2 = '&sort=' + sorts.map((value) => value.column + ',' + value.dir).join('&sort=');
  console.log(sortUrl);
  console.log(sortUrl2);
  return (
    <div>
      <div style={styles.container}>
        <div style={styles.left}>
            <RadioButtonGroup 
              name="shipSpeed" 
              defaultSelected="list"
              style={ {display: "flex"} }>
              <RadioButton
                checkedIcon={<ListIcon style={{ fill: '#35A2D0' }} />}
                uncheckedIcon={<ListIcon style={{ fill: '#7B7D7F' }} />}
                value="list"
              />
              <RadioButton
                checkedIcon={<TilesIcon style={{ fill: '#35A2D0' }} />}
                uncheckedIcon={<TilesIcon style={{ fill: '#7B7D7F' }} />}
                value="tiles"
              />
            </RadioButtonGroup>
        </div>
        <div style={styles.center}>
          <RaisedButton 
            label="Primary" 
            primary={true}
          />
        </div>
        <div style={styles.right}>
          rightrightrightrightrightrightrightrightrightright
        </div>
      </div>
      <div>
        <button onClick={onFetchUsers} type="button">
          Get users
        </button>
      </div>
      <div>
        {tracks.map(track => {
          return (
            <div className="track" key={track.alpha2Code}>
              {track.name}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Stream;
