import { shallow } from 'enzyme';
import { expect } from 'chai';
import Stream from './presenter';

describe('Stream', () => {
  const props = {
    tracks: [{ alpha2Code: 'x', name: 'X' }, { alpha2Code: 'y', name: 'Y' }]
  };

  it('shows two elements', () => {
    const element = shallow(<Stream {...props} />);

    expect(element.find('.track')).to.have.length(2);
  });
});
