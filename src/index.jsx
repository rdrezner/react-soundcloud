import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import configureStore from './stores/configureStore';
import Stream from './components/Stream';
import App from './components/App';
import SelectFieldExampleSelectionRenderer from './components/Multiselect';
import FiltersComponent from './components/Filters';
import ScrollspyComponent from './components/Scrollspy';
import RefsComponent from './components/Refs';

const store = configureStore();

const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
  <MuiThemeProvider>
    <Provider store={store}>
      <Router history={history}>
        <Route path="/" component={App}>
          <IndexRoute component={Stream} />
          <Route path="/" component={Stream} />
          <Route path="/multiselect" component={SelectFieldExampleSelectionRenderer} />
          <Route path="/filters" component={FiltersComponent} />
          <Route path="/scrollspy" component={ScrollspyComponent} />
          <Route path="/refs" component={RefsComponent} />
        </Route>
      </Router>
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('app')
);

module.hot.accept();
