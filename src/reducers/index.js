import { combineReducers } from 'redux';
import trackState from './track';
import { routerReducer } from 'react-router-redux';

const rootReducer = combineReducers({
  trackState,
  routing: routerReducer
});

export default rootReducer;