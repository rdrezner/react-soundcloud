import { createStore, applyMiddleware } from 'redux';
import rootReducer from './../reducers/index';
import { createLogger } from 'redux-logger';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';

const logger = createLogger();
const router = routerMiddleware(browserHistory);

const createStoreWithMiddleware = applyMiddleware(thunk, logger, router)(createStore);


const configureStore = (initialState) => {
  return createStoreWithMiddleware(rootReducer, initialState);
}

export default configureStore;